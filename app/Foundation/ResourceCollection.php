<?php


namespace App\Foundation;


use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection as BaseResourceCollection;

class ResourceCollection extends BaseResourceCollection
{
    /**
     * @var bool
     */
    private $status = true;

    /**
     * @param Request $request
     * @return array
     */
    public function with($request)
    {
        return [
            'status' => $this->isStatus()
        ];
    }

    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     *
     * @return ResourceCollection
     */
    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }
}
