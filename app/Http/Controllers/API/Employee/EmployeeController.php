<?php

namespace App\Http\Controllers\API\Employee;

use App\Http\Controllers\APIController;
use App\Http\Controllers\Controller;
use App\Http\Requests\EmployeeRequest;
use App\Http\Resources\EmployeeCollection;
use App\Http\Resources\EmployeeResource;
use App\Models\Employee;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class EmployeeController extends APIController
{
    /**
     * Display a listing of the resource.
     *
     * @return EmployeeCollection
     */
    public function index(): EmployeeCollection
    {
        $employeeList = Employee::latest()->paginate();

        return new EmployeeCollection($employeeList);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param EmployeeRequest $request
     *
     * @return EmployeeResource
     */
    public function store(EmployeeRequest $request): EmployeeResource
    {
        $validatedRequest = $request->validated();

        $employee = Employee::create($validatedRequest);

        return new EmployeeResource($employee);
    }

    /**
     * Display the specified resource.
     *
     * @param Employee $employee
     *
     * @return EmployeeResource
     */
    public function show(Employee $employee): EmployeeResource
    {
        return new EmployeeResource($employee);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EmployeeRequest $request
     * @param Employee $employee
     *
     * @return EmployeeResource
     */
    public function update(EmployeeRequest $request, Employee $employee): EmployeeResource
    {
        $validatedRequest = $request->validated();

        $employee->update($validatedRequest);

        return new EmployeeResource($employee->fresh());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Employee $employee
     *
     * @return JsonResponse
     */
    public function destroy(Employee $employee): JsonResponse
    {
        try {
            $employee->delete();
        } catch (\Exception $e) {
            $this->responseFailed(['error' => 'Something went wrong.'], 500);
        }

        return $this->responseSuccess(['message' => 'Employee with ID: ' . $employee->id . ' was deleted']);
    }
}
