<?php


namespace App\Http\Controllers\API;


use App\Http\Controllers\APIController;
use App\Http\Requests\FeedbackRequest;
use App\Http\Resources\FeedbackCollection;
use App\Http\Resources\FeedbackResource;
use App\Models\Employee;
use App\Models\Feedback;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class FeedbackController extends APIController
{
    /**
     * @return FeedbackCollection
     */
    public function index(): FeedbackCollection
    {
        $feedback = Feedback::latest()
            ->with('employee:id,first_name,last_name,phone,department,avatar')->paginate();

        return new FeedbackCollection($feedback);
    }

    /**
     * @param Request $request
     *
     * @return FeedbackCollection
     */
    public function indexEmployee(Request $request): FeedbackCollection
    {
        /** @var Employee $employee */
        $employee = Employee::where('phone', $request->phone)->firstOrFail();

        $feedback = $employee->feedbackTo()
            ->where('created_at', '>=', now()->subDays(2))
            ->get();

        return new FeedbackCollection($feedback);
    }

    /**
     * @param FeedbackRequest $request
     *
     * @return FeedbackResource
     */
    public function storeToEmployee(FeedbackRequest $request): FeedbackResource
    {
        /** @var Employee $employee */
        $employee = Employee::where('phone', $request->phone)->firstOrFail();

        $feedback = $employee->feedback()->create([
            'text' => $request->text,
            'is_from_employee' => false,
        ]);

        return new FeedbackResource($feedback);
    }

    /**
     * @param FeedbackRequest $request
     *
     * @return FeedbackResource
     */
    public function storeFromEmployee(FeedbackRequest $request): FeedbackResource
    {
        /** @var Employee $employee */
        $employee = Employee::where('phone', $request->phone)->firstOrFail();

        $feedback = $employee->feedback()->create([
            'text' => $request->text,
            'is_from_employee' => true,
        ]);

        return new FeedbackResource($feedback);
    }
}
