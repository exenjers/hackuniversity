<?php


namespace App\Http\Controllers\API\Test;


use App\Http\Controllers\APIController;
use App\Http\Requests\TestRequest;
use App\Http\Resources\TestCollection;
use App\Http\Resources\TestResource;
use App\Models\Employee;
use App\Models\Question;
use App\Models\Test;
use App\Services\Test\TestTemplate;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\JsonResponse;

class TestController extends APIController
{
    /**
     * @return TestCollection
     */
    public function index(): TestCollection
    {
        $tests = Test::all();

        return new TestCollection($tests);
    }

    /**
     * @param TestRequest $request
     *
     * @return TestResource
     */
    public function store(TestRequest $request): TestResource
    {
        $validatedRequest = $request->validated();

        $test = Test::create($validatedRequest);

        return new TestResource($test);
    }

    /**
     * @param Test $test
     *
     * @return TestResource
     */
    public function replicate(Test $test): TestResource
    {
        $template = (new TestTemplate($test))->replicateWithQuestions();

        return new TestResource($template);
    }

    /**
     * @return JsonResponse
     */
    public function entities(): JsonResponse
    {
        $employee = Employee::select('first_name', 'last_name', 'department')->orderBy('last_name')->get();
        $departments = $employee->pluck('department')->unique()->values();

        return $this->responseSuccess([
            'data' => [
                'employee' => $employee,
                'departments' => $departments
            ]
        ]);
    }

    /**
     * @param Test $test
     *
     * @return TestResource
     */
    public function show(Test $test): TestResource
    {
        $test->load('questions');

        return new TestResource($test);
    }

    /**
     * @param TestRequest $request
     * @param Test $test
     *
     * @return TestResource
     */
    public function update(TestRequest $request, Test $test): TestResource
    {
        $test->update($request->validated());

        return new TestResource($test->fresh());
    }

    /**
     * @param Test $test
     *
     * @return JsonResponse
     */
    public function destroy(Test $test): JsonResponse
    {
        try {
            $test->delete();
        } catch (\Exception $e) {
            $this->responseFailed(['error' => 'Something went wrong.'], 500);
        }

        return $this->responseSuccess(['message' => 'Test with ID: ' . $test->id . ' was deleted']);
    }
}
