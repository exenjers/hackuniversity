<?php


namespace App\Http\Controllers\API\Test;


use App\Http\Controllers\APIController;
use App\Http\Requests\QuestionRequest;
use App\Http\Resources\QuestionCollection;
use App\Http\Resources\QuestionResource;
use App\Models\Question;
use App\Models\Test;
use App\Services\Test\TestQuestionFactory;
use Illuminate\Http\JsonResponse;

class TestQuestionController extends APIController
{
    /**
     * @param Test $test
     *
     * @return QuestionCollection
     */
    public function index(Test $test): QuestionCollection
    {
        $questions = $test->questions;

        return new QuestionCollection($questions);
    }

    /**
     * @param Test $test
     * @param QuestionRequest $request
     *
     * @param TestQuestionFactory $testQuestionFactory
     * @return QuestionResource
     */
    public function store(
        Test $test, QuestionRequest $request, TestQuestionFactory $testQuestionFactory
    ): QuestionResource
    {
        $validatedRequest = $request->validated();

        $createRequest = $testQuestionFactory->createStoreArray($test, $validatedRequest);
        $question = $test->questions()->create($createRequest);

        return new QuestionResource($question);
    }

    /**
     * @param Test $test
     * @param Question $question
     * @param QuestionRequest $request
     *
     * @return QuestionResource
     */
    public function update(
        Test $test, Question $question, QuestionRequest $request
    ): QuestionResource
    {
        $question->update($request->validated());

        return new QuestionResource($question->fresh());
    }

    /**
     * @param Test $test
     * @param Question $question
     *
     * @return JsonResponse
     */
    public function destroy(Test $test, Question $question): JsonResponse
    {
        try {
            $question->delete();
        } catch (\Exception $e) {
            return $this->responseFailed(['error' => 'Something went wrong.'], 500);
        }

        return $this->responseSuccess(['message' => 'Question with ID: ' . $question->id . ' was deleted']);
    }
}
