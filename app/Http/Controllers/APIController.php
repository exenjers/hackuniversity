<?php


namespace App\Http\Controllers;


use Illuminate\Http\JsonResponse;

abstract class APIController extends Controller
{
    /**
     * @param array $data
     * @param int $status
     * @param array $headers
     * @return JsonResponse
     */
    protected function responseSuccess(
        array $data,
        int $status = 200,
        array $headers = ['Content-Type' => 'application/json;charset=utf8']
    ): JsonResponse
    {
        return response()->json(
            $this->setDataStatus($data), $status, $headers, JSON_UNESCAPED_UNICODE
        );
    }

    /**
     * @param array $data
     * @param int $status
     * @param array $headers
     * @return JsonResponse
     */
    protected function responseFailed(
        array $data,
        int $status = 400,
        array $headers = ['Content-Type' => 'application/json;charset=utf8']
    ): JsonResponse
    {
        return response()->json(
            $this->setDataStatus($data, false), $status, $headers, JSON_UNESCAPED_UNICODE
        );
    }

    /**
     * @param string $message
     * @return JsonResponse
     */
    protected function responseAccessDenied(string $message = 'Access denied.'): JsonResponse
    {
        return $this->responseFailed(['error' => $message], 403);
    }

    /**
     * @param array $data
     * @param bool $status
     * @return array
     */
    private function setDataStatus(array $data, bool $status = true): array
    {
        if (! isset($data['status'])) {
            $data['status'] = $status;

            return $data;
        }

        return $data;
    }
}
