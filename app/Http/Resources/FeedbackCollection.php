<?php

namespace App\Http\Resources;


use App\Foundation\ResourceCollection;

class FeedbackCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return FeedbackResource
     */
    public function toArray($request)
    {
        return new FeedbackResource($this->collection);
    }
}
