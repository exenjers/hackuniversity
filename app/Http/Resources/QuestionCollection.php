<?php

namespace App\Http\Resources;


use App\Foundation\ResourceCollection;

class QuestionCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return QuestionResource
     */
    public function toArray($request)
    {
        return new QuestionResource($this->collection);
    }
}
