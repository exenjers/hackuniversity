<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Employee extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'gender', 'department', 'phone', 'notes', 'hobbies', 'vk', 'headhunter', 'avatar'
    ];

    /**
     * @return HasMany
     */
    public function feedbackFrom(): HasMany
    {
        return $this->hasMany(Feedback::class)
            ->where('is_from_employee', true);
    }

    /**
     * @return HasMany
     */
    public function feedbackTo(): HasMany
    {
        return $this->hasMany(Feedback::class)
            ->where('is_from_employee', false);
    }

    /**
     * @return HasMany
     */
    public function feedback(): HasMany
    {
        return $this->hasMany(Feedback::class);
    }
}
