<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Feedback extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'employee_id', 'is_from_employee', 'text'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'from_employee' => 'boolean'
    ];

    /**
     * @return BelongsTo
     */
    public function employee(): BelongsTo
    {
        return $this->belongsTo(Employee::class);
    }
}
