<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Test extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'title', 'goal', 'started_at'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'started_at' => 'datetime'
    ];

    /**
     * @return HasMany
     */
    public function questions(): HasMany
    {
        return $this->hasMany(Question::class)->orderBy('index');
    }

    /**
     * @return HasMany
     */
    public function notIndexedQuestions(): HasMany
    {
        return $this->hasMany(Question::class);
    }
}
