<?php


namespace App\Repositories\Contracts;


use App\Models\Test;

interface TestRepositoryInterface
{
    /**
     * @param Test $test
     *
     * @return int|null
     */
    public function getMaxQuestionIndex(Test $test): ?int ;
}
