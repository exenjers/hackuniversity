<?php


namespace App\Repositories;


use App\Models\Test;
use App\Repositories\Contracts\TestRepositoryInterface;

class TestRepository implements TestRepositoryInterface
{
    /**
     * @param Test $test
     *
     * @return int|null
     */
    public function getMaxQuestionIndex(Test $test): ?int
    {
        return $test->questions()
            ->orderByDesc('index')
            ->max('index');
    }
}
