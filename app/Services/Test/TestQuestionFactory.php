<?php


namespace App\Services\Test;


use App\Models\Test;
use App\Repositories\Contracts\TestRepositoryInterface;

class TestQuestionFactory
{
    /**
     * @var TestRepositoryInterface
     */
    private $testRepository;

    /**
     * TestQuestionFactory constructor.
     *
     * @param TestRepositoryInterface $testRepository
     */
    public function __construct(TestRepositoryInterface $testRepository)
    {
        $this->testRepository = $testRepository;
    }

    /**
     * @param Test $test
     * @param array $validatedRequest
     *
     * @return array
     */
    public function createStoreArray(Test $test, array $validatedRequest): array
    {
        $maxIndex = $this->testRepository->getMaxQuestionIndex($test);

        if ($maxIndex === null) {
            $maxIndex = 1;
        }

        $createRequest = $validatedRequest;
        $createRequest['index'] = $maxIndex + 1;

        return $createRequest;
    }
}
