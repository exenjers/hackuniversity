<?php


namespace App\Services\Test;


use App\Models\Question;
use App\Models\Test;

class TestTemplate
{
    /**
     * @var Test
     */
    private $test;

    /**
     * TestTemplate constructor.
     *
     * @param Test $test
     */
    public function __construct(Test $test)
    {
        $this->test = $test;
    }

    /**
     * @return Test
     */
    public function replicateWithQuestions(): Test
    {
        $this->test->loadMissing('questions');

        $template = $this->test->replicate();
        $template->push();
        $template->fresh();

        /** @var Question $question */
        foreach ($this->test->questions as $question) {
            $templateQuestion = $question->replicate();
            $template->questions()->save($templateQuestion);
        }

        return $template;
    }
}
