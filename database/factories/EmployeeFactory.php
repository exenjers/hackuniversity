<?php

/** @var Factory $factory */

use App\Models\Employee;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Employee::class, function (Faker $faker) {
    $gender = $faker->randomElement(['Male', 'Female']);

    return [
        'first_name' => $faker->firstName($gender),
        'last_name' => $faker->lastName,
        'gender' => $gender[0],
        'phone' => '8-9' . rand(11, 49) . '-' . rand(100, 999) . '-' . rand(10, 99) . '-' . rand(10, 99),
        'department' => $faker->randomElement([
            'ИТ-отдел', 'Финансовый отдел', 'Отдел логистики', 'Отдел по качеству', 'Департамент продаж', 'Производство',
            'Материальное обеспечение'
        ]),
        'notes' => $faker->realText(),
        'hobbies' => $faker->text,
        'vk' => 'https://vk.com/id' . rand(199999999, 4999999999),
        'headhunter' => 'https://hh.ru/resume/' . strtolower(\Illuminate\Support\Str::random(8)),
    ];
});
