<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Feedback;
use Faker\Generator as Faker;

$factory->define(Feedback::class, function (Faker $faker) {
    return [
        'employee_id' => \App\Models\Employee::inRandomOrder()->first()->id,
        'is_from_employee' => $faker->boolean,
        'text' => $faker->realText()
    ];
});
