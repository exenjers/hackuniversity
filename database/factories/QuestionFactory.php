<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Question;
use App\Models\Test;
use Faker\Generator as Faker;

$factory->define(Question::class, function (Faker $faker) {
    return [
        'title' => $faker->text(20),
        'index' => rand(1, 5),
        'max_choices' => $faker->randomElement([2, 5]),
        'test_id' => Test::inRandomOrder()->first()->id,
    ];
});
