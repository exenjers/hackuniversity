<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Models\Test::class, function (Faker $faker) {
    return [
        'title' => $faker->text(20),
        'goal' => $faker->randomElement(['Эмоции', 'Компетенции', 'Роли'])
    ];
});
