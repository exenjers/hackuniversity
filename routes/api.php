<?php

use Illuminate\Support\Facades\Route;

// Employee
Route::apiResource('employee', 'Employee\EmployeeController');

// Test Questions
Route::apiResource('/tests/{test}/questions', 'Test\TestQuestionController')->except(['show']);

// Tests
Route::get('tests/entities', 'Test\TestController@entities');
Route::post('tests/{test}/replicate', 'Test\TestController@replicate');
Route::apiResource('tests', 'Test\TestController');

// Feedback
Route::group([
    'prefix' => 'feedback'
], function () {
    Route::get('/', 'FeedbackController@index');
    Route::post('employee', 'FeedbackController@indexEmployee');
    Route::post('from', 'FeedbackController@storeFromEmployee');
    Route::post('to', 'FeedbackController@storeToEmployee');
});
